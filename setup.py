#!/usr/bin/env python3

# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import re
from setuptools import setup

with open('condtest/__init__.py') as inp:
    VERSION = re.search(r'^__VERSION__\s*=\s*[\"\'](.*)[\"\']', inp.read(),
                        re.M).group(1)

setup(
    name='condtest',
    version=VERSION,
    author='Dirk Beyer',
    description='Framework for conditional software testing',
    url='https://gitlab.com/sosy-lab/software/conditional-testing',
    packages=['condtest'],
    scripts=['bin/reducer/pruner', 'bin/reducer/identity', 'bin/reducer/annotator', 'bin/extractor/test_executor', 'bin/instrumenter/instrumenter'],
    install_requires=[
        'psutil>=5.6.2',
        'benchexec>=1.20',
        'pycparser>=1.12',
    ],
    setup_requires=[
    ],
    tests_require=[
    ],
    license='Apache-2.0',
    keywords='test-case generation verification',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'Operation System :: POSIX :: Linux',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Software Development :: Testing',
    ],
    platforms=['Linux'],
)
