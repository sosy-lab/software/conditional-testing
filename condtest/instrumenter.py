# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import argparse
import re
import logging
from pathlib import Path
from typing import Optional
import pycparser

import condtest
from condtest import utils


class _LabelAdder(pycparser.c_ast.NodeVisitor):
    """Add labels at each branch on the visited AST, in-situ."""

    def __init__(self, args, optimize_labels=True):
        del args  # not used at the moment
        self._labels = 0
        self.added = set()
        self._optimize = optimize_labels

    def _get_label(self) -> pycparser.c_ast.Label:
        name = utils.LABEL_PREFIX + str(self._labels)
        self._labels += 1
        label = pycparser.c_ast.Label(name, stmt=pycparser.c_ast.EmptyStatement())
        self.added.add(label)
        return label

    def _add_label_at_start(
        self, node: Optional[pycparser.c_ast.Node]
    ) -> pycparser.c_ast.Node:
        if isinstance(node, pycparser.c_ast.Compound):
            if node.block_items is None:
                node.block_items = list()
            node.block_items.insert(0, self._get_label())
            return node

        return self._add_label_at_start(
            pycparser.c_ast.Compound([node] if node else [])
        )

    def visit_If(self, node):
        # Visit children before adding labels to work on original AST
        self.generic_visit(node)

        if self._optimize and self._followed_by_if(node.iftrue):
            pass
        else:
            node.iftrue = self._add_label_at_start(node.iftrue)

        if self._optimize and self._followed_by_if(node.iffalse):
            pass
        else:
            node.iffalse = self._add_label_at_start(node.iffalse)

    @staticmethod
    def _followed_by_if(node) -> bool:
        return isinstance(node, pycparser.c_ast.If) or (
            isinstance(node, pycparser.c_ast.Compound)
            and node.block_items
            and len(node.block_items) == 1
            and isinstance(node.block_items[0], pycparser.c_ast.If)
        )

    def visit_Compound(self, node):
        self.generic_visit(node)
        if not node.block_items:
            return

        i = 0
        while i < len(node.block_items):
            stmt = node.block_items[i]
            if isinstance(
                stmt,
                (pycparser.c_ast.While, pycparser.c_ast.For, pycparser.c_ast.DoWhile),
            ):
                node.block_items.insert(i + 1, self._get_label())
                i = i + 2
            else:
                i = i + 1

    def visit_While(self, node):
        self.generic_visit(node)
        node.stmt = self._add_label_at_start(node.stmt)

    def visit_DoWhile(self, node):
        self.generic_visit(node)
        node.stmt = self._add_label_at_start(node.stmt)

    def visit_For(self, node):
        self.generic_visit(node)
        node.stmt = self._add_label_at_start(node.stmt)

    def visit_Case(self, node):
        self.generic_visit(node)
        if node.stmts:
            node.stmts[0] = self._add_label_at_start(node.stmts[0])
        # we don't add a label if the case has no own content but 'falls through'

    def visit_Default(self, node):
        self.generic_visit(node)
        node.stmts[0] = self._add_label_at_start(node.stmts[0])

    def visit_FuncDef(self, node):
        self.generic_visit(node)
        if node.decl.name == "main":
            # entry of main is always covered ; we also need this
            # in case the very first statement is a input method.
            # In that case, a model checker may produce an empty test,
            # which makes the test executor hang at the first statement
            # and report no coverage. This will keep it stuck at that first goal.
            return
        if node.body is None:
            logging.debug(
                "Function %s has no body, adding compound statement.", node.decl.name
            )
            node.body = pycparser.c_ast.Compound(list())
        self._add_label_at_start(node.body)


def instrument(program_file, args) -> str:
    content = utils.get_content(program_file)
    content = re.sub(r"(extern\s+)?void\s+abort\s*\(.*\)\s*;", "", content)
    content = content.replace("abort();", "exit(42);")
    content = _remove_error_calls(content, args)
    ast = utils.parse(content, args)
    _add_goal_labels(ast, args)
    return utils.to_c(ast)


def _add_goal_labels(ast, args) -> pycparser.c_ast.FileAST:
    logging.debug("Adding program labels")
    adder = _LabelAdder(args)
    adder.visit(ast)
    all_labels = list(label.name for label in adder.added)
    logging.debug("Finished adding program labels (%s)", len(all_labels))
    return ast


def _remove_error_calls(program_content: str, args) -> str:
    del args  # unused at the moment
    if "__VERIFIER_error" in program_content:
        return (
            "void __VERIFIER_error();\nvoid __originalError() { exit(1); }\n"
            + program_content.replace("__VERIFIER_error", "__originalError")
        )
    return program_content


def _parse_args(arguments) -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--version", action="version", version=f"{condtest.__VERSION__}"
    )
    parser.add_argument(
        "-o",
        "--output",
        dest="output_file",
        action=utils.StorePath,
        default=None,
        help="Output file. If none is given, output is written to stdout",
    )
    parser.add_argument(
        "--spec",
        action=utils.StoreSpecification,
        default=None,
        required=True,
        help="test-goal specification to instrument program for",
    )
    parser.add_argument(
        "--debug", action="store_true", default=False, help="Show debug output"
    )
    parser.add_argument("program_file", action=utils.StoreInputPath)

    args = parser.parse_args(arguments)
    setattr(
        args, "output_dir", args.output_file.parent if args.output_file else Path(".")
    )

    return args


def main(arguments):
    args = _parse_args(arguments)
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)
    content = instrument(args.program_file, args)
    if args.output_file is None:
        print(content)
    else:
        with args.output_file.open("w") as outp:
            outp.write(content)
    return 0
