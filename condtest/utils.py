# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import argparse
import logging
import subprocess
import os
import re
import resource
import signal
from pathlib import Path
from typing import Optional, Union, Callable

import psutil
import pycparser
import pycparser.c_generator


class ConfigError(Exception):
    def __init__(self, msg=None, cause=None):
        super().__init__(msg, cause)
        self.msg = msg
        self.cause = cause


class ParseError(Exception):
    def __init__(self, msg=None, cause=None):
        super().__init__(msg, cause)
        self.msg = msg
        self.cause = cause


class StoreSpecification(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super().__init__(option_strings, dest, nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        del parser, option_string
        if values is not None:
            spec = parse_spec(Path(values))
            setattr(namespace, self.dest, spec)


class StorePath(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        super().__init__(option_strings, dest, nargs, **kwargs)

    @staticmethod
    def create_path(path) -> Path:
        return Path(path)

    def __call__(self, parser, namespace, values, option_string=None):
        del parser, option_string
        paths = None
        if values is not None:
            if isinstance(values, str):
                paths = self.create_path(values)
            else:
                paths = [self.create_path(v) for v in values]
        setattr(namespace, self.dest, paths)


class StoreInputPath(StorePath):
    @staticmethod
    def create_path(path) -> Path:
        p = Path(path)
        if not p.exists():
            raise ValueError("Given path %s does not exist" % str(p))
        return p


def initialize_logger(name, debug: bool = False, output_dir: Path = None):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    if output_dir:
        file_handler = logging.FileHandler(output_dir / "condtest.log")
        file_handler.setLevel(logging.DEBUG)
        file_formatter = logging.Formatter("%(asctime)s::%(levelname)s::%(message)s")
        file_handler.setFormatter(file_formatter)

        logger.addHandler(file_handler)

    if debug:
        console_log_lvl = logging.INFO
    else:
        console_log_lvl = logging.WARNING
    console_handler = logging.StreamHandler()
    console_handler.setLevel(console_log_lvl)
    console_formatter = logging.Formatter("%(levelname)s: %(message)s")
    console_handler.setFormatter(console_formatter)

    logger.addHandler(console_handler)
    return logger


class ExecutionResult:
    """Results of a subprocess execution."""

    def __init__(self, returncode, stdout, stderr):
        self._returncode = returncode
        self._stdout = stdout
        self._stderr = stderr

    @property
    def returncode(self):
        return self._returncode

    @property
    def stdout(self):
        return self._stdout

    @property
    def stderr(self):
        return self._stderr


def limit_memory(memlimit_in_gb):
    gb_to_byte = lambda n: n * 1000 * 1000 * 1000
    memlimit_in_byte = gb_to_byte(memlimit_in_gb)
    resource.setrlimit(resource.RLIMIT_AS, (memlimit_in_byte, memlimit_in_byte))


def execute(
    command,
    quiet=False,
    env=None,
    err_to_output=True,
    input_str=None,
    timelimit: Optional[int] = None,
    show_output=False,
    memlimit: Optional[float] = None,  # in GB
):

    log_cmd = logging.debug if quiet else logging.info

    if env:
        logging.debug("PATH=%s", env["PATH"])
        logging.debug(
            "LD_LIBRARY_PATH=%s",
            env["LD_LIBRARY_PATH"] if "LD_LIBRARY_PATH" in env else "[]",
        )
    log_cmd(" ".join(command))

    p = None
    try:

        def limit_resources():
            if memlimit:
                limit_memory(memlimit)

        p = psutil.Popen(
            command,
            stdin=subprocess.PIPE if input_str else None,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT if err_to_output else subprocess.PIPE,
            universal_newlines=False,
            env=env,
            preexec_fn=limit_resources,
        )

        if input_str and not isinstance(input_str, bytes):
            input_str = input_str.encode()

        output, err_output = b"", b""
        try:
            output, err_output = p.communicate(input=input_str, timeout=timelimit)
        except subprocess.TimeoutExpired:
            processes = [p] + p.children(recursive=True)
            logging.info(
                "Timelimit reached, sending SIGINT to %s", [q.pid for q in processes]
            )
            for proc in processes:
                os.kill(proc.pid, signal.SIGINT)
            _, alive = psutil.wait_procs(processes, timeout=3)
            if alive:
                for a in alive:
                    logging.info("SIGINT didn't work on %s, terminating process", a.pid)
                    a.terminate()
            try:
                output, err_output = p.communicate(timeout=2)
            except subprocess.TimeoutExpired:
                logging.debug("Process hasn't stopped yet, continuing.")
        finally:
            log_output = logging.info if show_output else logging.debug
            for outp in (output, err_output):
                if outp:
                    try:
                        log_output(outp.decode())
                    except UnicodeDecodeError:
                        log_output(outp)

        returncode = p.poll()
        return ExecutionResult(returncode, output, err_output)
    finally:
        if p and p.is_running():
            logging.debug("Terminating current process")
            procs = [p] + p.children()
            for proc in procs:
                try:
                    proc.terminate()
                except psutil.NoSuchProcess:
                    # Process died concurrently
                    pass
            _, alive = psutil.wait_procs(procs, timeout=3)
            if alive:
                logging.debug("Not terminated yet. Killing current process")
                for proc in alive:
                    try:
                        proc.kill()
                    except psutil.NoSuchProcess:
                        # Process died concurrently
                        pass


def parse_spec(specification_file: Path) -> Path:
    with specification_file.open() as inp:
        content = inp.read()
    if is_branch_coverage(content):
        return specification_file
    raise ConfigError("Only branch coverage supported at the moment")


def is_branch_coverage(spec_str: str) -> bool:
    return (
        re.match(
            r"cover\(\s*init\(\s*main\(\)\s*\),\s*fql\(\s*cover\s+edges\(\s*@decisionedge\s*\)\s*\)\s*\)",
            spec_str.strip().lower(),
        )
        is not None
    )


LABEL_PREFIX = "GOAL_"


def get_content(program: Union[Path, str]) -> str:
    if isinstance(program, Path):
        with program.open() as inp:
            return inp.read()
    else:
        return program


def _get_parser(args) -> pycparser.c_parser.CParser:
    del args  # unused
    return pycparser.c_parser.CParser()


def parse(content_original: str, args) -> pycparser.c_ast.FileAST:
    logging.debug("Parsing program")
    try:
        content = _rewrite_cproblems(content_original)

        parser = _get_parser(args)
        #  we don't specify a filename because the original file may be
        # different from the one given to pycparser (because of rewrites)
        return parser.parse(content)
    finally:
        logging.debug("Finished parsing program")


def _rewrite_cproblems(content: str) -> str:
    need_struct_body = False
    skip_asm = False
    in_attribute = False
    prepared_content = ""
    for line in [c + "\n" for c in content.split("\n")]:
        line = re.sub(r"/\*.*?\*/", "", line)
        # remove __attribute__
        line = re.sub(r"__attribute__\s*\(\(\s*[a-z_, ]+\s*\)\)\s*", "", line)
        # line = re.sub(r'__attribute__\s*\(\(\s*[a-z_, ]+\s*\(\s*[a-zA-Z0-9_, "\.]+\s*\)\s*\)\)\s*', '', line)
        # line = re.sub(r'__attribute__\s*\(\(\s*[a-z_, ]+\s*\(\s*sizeof\s*\([a-z ]+\)\s*\)\s*\)\)\s*', '', line)
        # line = re.sub(r'__attribute__\s*\(\(\s*[a-z_, ]+\s*\(\s*\([0-9]+\)\s*<<\s*\([0-9]+\)\s*\)\s*\)\)\s*', '', line)
        line = re.sub(r"__attribute__\s*\(\(.*\)\)\s*", "", line)
        if re.search(r"__attribute__\s*\(\(", line):
            line = re.sub(r"__attribute__\s*\(\(.*", "", line)
            in_attribute = True
        elif in_attribute:
            line = re.sub(r".*\)\)", "", line)
            in_attribute = False
        # rewrite some GCC extensions
        line = re.sub(r"__extension__", "", line)
        line = re.sub(r"__restrict", "", line)
        line = re.sub(r"__restrict__", "", line)
        line = re.sub(r"__inline__", "", line)
        line = re.sub(r"__inline", "", line)
        line = re.sub(r"__const", "const", line)
        line = re.sub(r"__signed__", "signed", line)
        line = re.sub(r"__builtin_va_list", "int", line)
        # a hack for some C-standards violating code in LDV benchmarks
        if need_struct_body and re.match(r"^\s*}\s*;\s*$", line):
            line = "int __dummy; " + line
            need_struct_body = False
        elif need_struct_body:
            need_struct_body = re.match(r"^\s*$", line) is not None
        elif re.match(r"^\s*struct\s+[a-zA-Z0-9_]+\s*{\s*$", line):
            need_struct_body = True
        # remove inline asm
        if re.match(r'^\s*__asm__(\s+volatile)?\s*\("([^"]|\\")*"[^;]*$', line):
            skip_asm = True
        elif skip_asm and re.search(r"\)\s*;\s*$", line):
            skip_asm = False
            line = "\n"
        if skip_asm or re.match(
            r'^\s*__asm__(\s+volatile)?\s*\("([^"]|\\")*"[^;]*\)\s*;\s*$', line
        ):
            line = "\n"
        # remove asm renaming
        line = re.sub(r'__asm__\s*\(""\s+"[a-zA-Z0-9_]+"\)', "", line)
        prepared_content += line

    def replacer(match):
        s = match.group(0)
        if s.startswith("/"):
            return ""
        return s

    pattern = re.compile(
        r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
        re.DOTALL | re.MULTILINE,
    )
    return re.sub(pattern, replacer, prepared_content)


def to_c(ast: pycparser.c_ast.Node) -> str:
    generator = CondensingCGenerator()
    return generator.visit(ast)


def call_on_ast(
    func: Callable[[pycparser.c_ast.FileAST], pycparser.c_ast.FileAST],
    program_file: Path,
    args=None,
) -> str:
    with program_file.open() as inp:
        content = inp.read()
    ast = parse(content, args)
    result = func(ast)
    return to_c(result)


class CondensingCGenerator(pycparser.c_generator.CGenerator):
    def visit_Label(self, n):
        if isinstance(n.stmt, pycparser.c_ast.EmptyStatement):
            separator = ""
        else:
            separator = "\n"
        return n.name + ":" + separator + self._generate_stmt(n.stmt).strip()

    def visit_If(self, n):
        s = "if ("
        if n.cond:
            s += self.visit(n.cond)
        s += ") "
        s += self._generate_stmt(n.iftrue, add_indent=True)
        if n.iffalse:
            s += self._make_indent() + "else"
            s += self._generate_stmt(n.iffalse, add_indent=True)
        return s

    def visit_While(self, n):
        s = "while ("
        if n.cond:
            s += self.visit(n.cond)
        s += ") "
        s += self._generate_stmt(n.stmt, add_indent=True)
        return s

    def visit_DoWhile(self, n):
        s = "do "
        s += self._generate_stmt(n.stmt, add_indent=True)
        s += self._make_indent() + "while ("
        if n.cond:
            s += self.visit(n.cond)
        s += ");"
        return s

    def visit_For(self, n):
        s = "for ("
        if n.init:
            s += self.visit(n.init)
        s += ";"
        if n.cond:
            s += " " + self.visit(n.cond)
        s += ";"
        if n.next:
            s += " " + self.visit(n.next)
        s += ") "
        s += self._generate_stmt(n.stmt, add_indent=True)
        return s
