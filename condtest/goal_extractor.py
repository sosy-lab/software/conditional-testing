# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import argparse
import logging
import os
import re
import sys
from typing import List, Iterable
from pathlib import Path

import condtest
from condtest import utils

MACHINE_MODEL = "-32"

LINE_COVERAGE_PROP = (Path(__file__).parent / "coverage-statements.prp").resolve()
# we always use line coverage because we are only interested in the covered labels,
# no matter which goal specification is given by the user
EXECUTION_PARAMS = [
    "--no-individual-test-coverage",
    "--timelimit-per-run",
    "5",
    MACHINE_MODEL,
    "--no-isolation",
    "--goal",
    str(LINE_COVERAGE_PROP),
]
EXECUTION_OUTPUT_DIR = Path("output")
"""Output directory of test executions. This is determined by tbf-testsuite-validator."""

GCOV_PARAMS: List[str] = []
GCOV_DATA_FILE = Path(
    "harness.gcda"
)  # this is always generated in the current working directory


# Allow label + ':' and label + ';' to also catch gotos, since these jump *behind* the label
# and thus make gcov not capture the label as covered (if on its own code line)
GCOV_BLOCK_REGEX = re.compile(r"[0-9]:\s*[0-9]+:\s*.*(GOAL_[0-9]+)[:;]")


def get_gcov_file(program_name):
    return program_name + ".gcov"


def get_test_executor_bin(args):
    return os.path.join(args.test_format_dir, "bin", "testcov")


def _execute_test(program: Path, test_suite_zip: Path, args) -> None:
    logging.debug("Executing test suite: %s", test_suite_zip.name)
    executor_bin = get_test_executor_bin(args)
    cmd = (
        [executor_bin, "--test-suite", str(test_suite_zip)]
        + EXECUTION_PARAMS
        + [str(program)]
    )

    utils.execute(cmd, show_output=True)


def extract_covered_goals(program: Path, test_suite_zip: Path, args) -> Iterable[str]:
    _execute_test(program, test_suite_zip, args)
    return _get_covered_specs(program, GCOV_DATA_FILE)


def _write_covered_goals_to_file(goals: Iterable[str], output_file: Path) -> None:
    prepared_goals = sorted([s + "\n" for s in goals])

    with output_file.open("w") as outp:
        outp.writelines(prepared_goals)


def _get_covered_specs(program: Path, gcov_data_file: Path) -> Iterable[str]:
    if not gcov_data_file.exists():
        logging.warning("No gcov data file %s", str(gcov_data_file))
        return set()

    cmd = ["gcov", str(gcov_data_file)]
    utils.execute(cmd, quiet=False, show_output=False)
    gcov_report = Path(program.name + ".gcov")
    if not gcov_report.exists():
        logging.warning("No gcov report exists: %s", str(gcov_report))
        return set()

    with open(gcov_report) as inp:
        return set(match.group(1) for match in GCOV_BLOCK_REGEX.finditer(inp.read()))


def _parse_args(arguments) -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--version", action="version", version=f"{condtest.__VERSION__}"
    )
    parser.add_argument(
        "-o",
        "--output",
        dest="output_file",
        action=utils.StorePath,
        default=None,
        help="Output file. If none is given, output is written to stdout",
    )
    parser.add_argument(
        "--test-suite",
        action=utils.StorePath,
        required=True,
        help="test suite as zip file",
    )
    parser.add_argument(
        "--spec",
        action=utils.StoreSpecification,
        default=None,
        required=True,
        help="test-goal specification to instrument program for",
    )
    parser.add_argument(
        "--debug", action="store_true", default=False, help="Show debug output"
    )
    parser.add_argument("program_file", action=utils.StoreInputPath)

    args = parser.parse_args(arguments)

    if "TOOLS_DIRS" in os.environ:
        args.tools_dir = Path(os.environ["TOOLS_DIR"]).resolve()
    else:
        args.tools_dir = (Path(__file__) / ".." / ".." / Path("tools")).resolve()

    if "TEST_FORMAT_DIR" in os.environ:
        args.test_format_dir = Path(os.environ["TEST_FORMAT_DIR"]).resolve()
    else:
        args.test_format_dir = args.tools_dir / Path("testcov")

    return args


def main():
    args = _parse_args(sys.argv[1:])
    setattr(
        args, "output_dir", args.output_file.parent if args.output_file else Path(".")
    )
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)
    goals = extract_covered_goals(args.program_file, args.test_suite, args)
    if args.output_file is None:
        print("\n".join(g for g in goals))
    else:
        _write_covered_goals_to_file(goals, args.output_file)
    return 0
