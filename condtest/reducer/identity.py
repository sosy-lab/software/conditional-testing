# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

"""Identity reducer; always returns original program."""

from pathlib import Path
from typing import Iterable
from condtest.reducer import Reducer


class IdentityReducer(Reducer):
    def reduce_program(
        self, program_file: Path, irrelevant_labels: Iterable[str], args=None
    ) -> str:
        del irrelevant_labels
        del args
        with program_file.open() as inp:
            return inp.read()
