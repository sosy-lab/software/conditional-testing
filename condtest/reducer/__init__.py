# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

"""Different reducers that can be used for conditional testing."""

import argparse
from pathlib import Path
from typing import Iterable, Sequence

import condtest
from condtest import utils


def get_available() -> Iterable[str]:
    """Returns available reducers."""
    return [
        m.name[:-3]
        for p in __path__
        for m in Path(p).glob("*.py")
        if m.name != "__init__.py"
    ]


class Reducer:
    def __init__(self, logger):
        self.logger = logger

    def reduce_program(
        self, program_file: Path, irrelevant_labels: Iterable[str], args=None
    ) -> str:
        raise NotImplementedError()


def get_version() -> str:
    return f"{condtest.__VERSION__}"


def _parse_args(arguments) -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", action="version", version=get_version())
    parser.add_argument(
        "-o",
        "--output",
        dest="output_file",
        action=utils.StorePath,
        default=None,
        help="Output file. If none is given, output is written to stdout",
    )
    parser.add_argument(
        "--covered-labels",
        dest="irrelevant_labels_file",
        action=utils.StoreInputPath,
        required=False,
        help="File containing program labels already covered, one per line",
    )
    parser.add_argument(
        "--spec",
        action=utils.StoreSpecification,
        default=None,
        required=True,
        help="test-goal specification to instrument program for",
    )
    parser.add_argument(
        "--debug", action="store_true", default=False, help="Show debug output"
    )
    parser.add_argument("program_file", action=utils.StoreInputPath)

    args = parser.parse_args(arguments)
    return args


def _parse_irrelevant_labels(label_file: Path) -> Iterable[str]:
    with label_file.open() as inp:
        return [line.strip() for line in inp.readlines()]


def main(reducer_class: type, arguments: Sequence[str]):
    args = _parse_args(arguments)
    setattr(
        args, "output_dir", args.output_file.parent if args.output_file else Path(".")
    )
    logger = utils.initialize_logger(
        reducer_class.__name__, args.debug, args.output_dir
    )
    reducer = reducer_class(logger)
    if args.irrelevant_labels_file:
        irrelevant_labels = _parse_irrelevant_labels(args.irrelevant_labels_file)
    else:
        irrelevant_labels = list()
    reduced = reducer.reduce_program(args.program_file, irrelevant_labels, args)
    if args.output_file is None:
        print(reduced)
    else:
        with args.output_file.open("w") as outp:
            outp.write(reduced)
    return 0
