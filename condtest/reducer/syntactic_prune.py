# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

"""
Pruning reducer based on syntactic reachability.
Inserts an exit-statement in front of each code block
from which no remaining test goal is syntactically reachable.
"""

import logging
import functools
from pathlib import Path
from typing import Sequence, Iterable, Set, List
import pycparser
import condtest.reducer.cfg as cfg
from condtest.reducer.cfg import CfgNode
from condtest.reducer import Reducer
from condtest import utils


class PruningReducer(Reducer):
    def reduce_program(
        self, program_file: Path, irrelevant_labels: Sequence[str], args=None
    ) -> str:
        reduce_ast = functools.partial(
            self._reduce, irrelevant_labels=irrelevant_labels, args=args
        )
        return utils.call_on_ast(reduce_ast, program_file, args)

    def _reduce(
        self, ast: pycparser.c_ast.FileAST, irrelevant_labels: Sequence[str], args=None
    ) -> pycparser.c_ast.FileAST:
        prunable_labels = self._get_prunable_labels(ast, irrelevant_labels, args)
        logging.debug("Prunable labels: %s", irrelevant_labels)
        self._add_aborts(ast, prunable_labels, args)

        return ast

    @staticmethod
    def _get_all_labels(ast, args) -> Iterable[str]:
        class _LabelCollector(pycparser.c_ast.NodeVisitor):
            """Collects all labels in the ast"""

            def __init__(self, args):
                del args  # not used at the moment
                self.labels = list()  # use list for determinism

            def visit_Label(self, node):
                label = node.name
                if label.startswith(utils.LABEL_PREFIX):
                    self.labels.append(label)
                self.generic_visit(node)

        collector = _LabelCollector(args)
        collector.visit(ast)
        return collector.labels

    def _get_prunable_labels(
        self, ast: pycparser.c_ast.FileAST, irrelevant_labels: Iterable[str], args,
    ) -> Set[str]:
        all_labels = self._get_all_labels(ast, args)
        relevant_labels = set(all_labels) - set(irrelevant_labels)
        logging.debug("Building CFG")
        program_cfg = cfg.build_cfg(ast, args)
        logging.debug("Finished building CFG")
        logging.debug("Computing reachable nodes")
        reaching: List[CfgNode] = list(
            self._get_nodes_reaching(program_cfg, relevant_labels)
        )
        logging.debug("Finished computing reachable nodes (%s)", len(reaching))
        logging.debug("Computing reachable labels from nodes")
        labels_reaching = list(
            (
                label.name
                for n in reaching
                for label in n.asts
                if isinstance(label, pycparser.c_ast.Label)
            )
        )
        logging.debug(
            "Finished computing reachable labels from nodes (%s)", len(labels_reaching)
        )
        logging.debug("Reachable labels: %s", labels_reaching)
        logging.debug("All labels: %s", all_labels)
        return set(all_labels) - set(labels_reaching)

    def _get_nodes_reaching(
        self, program_cfg: CfgNode, labels: Iterable[str]
    ) -> Iterable[CfgNode]:
        logging.debug("Computing target nodes")
        cfg_targets = list(self._get_targets(program_cfg, labels))
        logging.debug("Finished computing target nodes (%s)", len(cfg_targets))
        waitlist: List[CfgNode] = list(cfg_targets)
        visited: Set[pycparser.c_ast.Node] = set()
        while waitlist:
            next_node = waitlist.pop()
            if next_node in visited:
                continue
            visited.add(next_node)
            waitlist += next_node.preds
            yield next_node

    @staticmethod
    def _get_targets(
        program_cfg: CfgNode, labels: Iterable[str]
    ) -> Iterable[pycparser.c_ast.Node]:
        labels = set(labels)
        waitlist: List[CfgNode] = [program_cfg]
        del program_cfg  # don't use anymore
        visited: Set[CfgNode] = set()
        while waitlist:
            next_node = waitlist.pop()
            if next_node in visited:
                continue
            visited.add(next_node)
            waitlist += next_node.all_succs
            if any(
                isinstance(a, pycparser.c_ast.Label) and a.name in labels
                for a in next_node.asts
            ):
                yield next_node

    @staticmethod
    def _add_aborts(ast: pycparser.c_ast.FileAST, labels: Iterable[str], args) -> None:
        logging.debug("Adding aborts")
        _AbortAdder(labels, args).visit(ast)
        logging.debug("Finished adding aborts")


class _AbortAdder(pycparser.c_ast.NodeVisitor):
    """Add an abort after each label with a name in the list given in the constructor, in-situ."""

    def __init__(self, labels: Iterable[str], args):
        del args  # not used at the moment
        self._labels = set(labels)

    def visit_Label(self, node):
        if node.name in self._labels:
            if node.stmt is not None and not isinstance(
                node.stmt, pycparser.c_ast.EmptyStatement
            ):
                old_stmt = node.stmt
                node.stmt = pycparser.c_ast.Compound([])
                node.stmt.block_items += self._get_abort_call(), old_stmt
            else:
                node.stmt = self._get_abort_call()
        else:
            self.generic_visit(node)

    @staticmethod
    def _get_abort_call() -> pycparser.c_ast.FuncCall:
        return pycparser.c_ast.FuncCall(pycparser.c_ast.ID("abort"), args=None)
