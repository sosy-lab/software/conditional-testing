# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from pathlib import Path
from typing import Iterable, Set
import pycparser
from condtest.reducer import Reducer
from condtest import utils


class AnnotatingReducer(Reducer):

    ANNOTATION_METHOD = "__VERIFIER_error"

    def reduce_program(
        self, program_file: Path, irrelevant_labels: Iterable[str], args=None
    ) -> str:
        with program_file.open() as inp:
            original_content = inp.read()
        relevant_labels = self._get_relevant(program_file, irrelevant_labels, args)
        annotated_content = self._inject_calls(
            original_content, relevant_labels, AnnotatingReducer.ANNOTATION_METHOD
        )
        return annotated_content

    @staticmethod
    def _get_all_labels(ast, args) -> Iterable[str]:
        class _LabelCollector(pycparser.c_ast.NodeVisitor):
            """Collects all labels in the ast"""

            def __init__(self, args):
                del args  # not used at the moment
                self.labels = list()  # use list for determinism

            def visit_Label(self, node):
                label = node.name
                if label.startswith(utils.LABEL_PREFIX):
                    self.labels.append(label)
                self.generic_visit(node)

        collector = _LabelCollector(args)
        collector.visit(ast)
        return collector.labels

    def _get_relevant(
        self, program_file: Path, irrelevant_labels: Iterable[str], args,
    ) -> Set[str]:
        with program_file.open() as inp:
            ast = utils.parse(inp.read(), args)
        all_labels = self._get_all_labels(ast, args)
        return set(all_labels) - set(irrelevant_labels)

    @staticmethod
    def _inject_calls(
        program_content: str, relevant_labels: Iterable[str], method_name: str
    ) -> str:
        new_lines = list()
        for line in program_content.split("\n"):
            for label in relevant_labels:
                old_label_line = label + ":"
                if old_label_line in line:
                    new_label_line = label + ": " + method_name + "();"
                    line = line.replace(old_label_line, new_label_line)
            new_lines.append(line)

        injected_program_content = "\n".join(new_lines)
        if method_name + "(" not in program_content:
            injected_program_content = (
                "extern void " + method_name + "();\n" + injected_program_content
            )

        return injected_program_content
