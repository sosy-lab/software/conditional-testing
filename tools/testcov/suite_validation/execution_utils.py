# This file is part of TestCov,
# a robust test executor with reliable coverage measurement:
# https://gitlab.com/sosy-lab/software/test-suite-validator/
#
# Copyright (C) 2018 - 2020  Dirk Beyer
# SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import logging
import subprocess
import time
from typing import List

ERROR_STRING = "Error found."

COVER_LINES = "COVER( init(main()), FQL(COVER EDGES(@BASICBLOCKENTRY)) )"
COVER_BRANCHES = "COVER( init(main()), FQL(COVER EDGES(@DECISIONEDGE)) )"
COVER_CONDITIONS = "COVER( init(main()), FQL(COVER EDGES(@CONDITIONEDGE)) )"
COVER_ERRORS = "COVER( init(main()), FQL(COVER EDGES(@CALL(__VERIFIER_error))) )"

COVERAGE_GOALS = {
    "@DECISIONEDGE": COVER_BRANCHES,
    "@CONDITIONEDGE": COVER_CONDITIONS,
    "@BASICBLOCKENTRY": COVER_LINES,
    "@CALL(__VERIFIER_error)": COVER_ERRORS,
}

MACHINE_MODEL_32 = "-m32"
MACHINE_MODEL_64 = "-m64"

COMPILER = "gcc"

EXTERNAL_DECLARATIONS = [
    ("_IO_FILE", "struct _IO_FILE;", "#include<stdio.h>;"),
    ("FILE", "typedef struct _IO_FILE FILE;", "#include<stdio.h>;"),
    ("stdin", "extern struct _IO_FILE *stdin;", "#include<stdio.h>;"),
    ("stderr", "extern struct _IO_FILE *stderr;", "#include<stdio.h>;"),
    ("size_t", "typedef long unsigned int size_t;", "#include<stddef.h>;"),
    (
        "abort",
        "extern void abort (void) __attribute__ ((__nothrow__ , __leaf__))"
        + " __attribute__ ((__noreturn__));",
        "#include<stdlib.h>;",
    ),
    (
        "exit",
        "extern void exit (int __status) __attribute__ ((__nothrow__ , __leaf__))"
        + " __attribute__ ((__noreturn__));",
        "#include<stdlib.h>;",
    ),
    (
        "fgets",
        "extern char *fgets (char *__restrict __s, int __n, FILE *__restrict __stream);",
        "#include<stdio.h>;",
    ),
    (
        "sscanf",
        "extern int sscanf (const char *__restrict __s, const char *__restrict __format, ...)"
        + " __attribute__ ((__nothrow__ , __leaf__));",
        "#include<stdio.h>;",
    ),
    (
        "strlen",
        " extern size_t strlen (const char *__s __attribute__ ((__nothrow__ , __leaf__))"
        + " __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))));",
        "#include<string.h>;",
    ),
    (
        "fprintf",
        "extern int fprintf (FILE *__restrict __stream, const char *__restrict __format, ...);",
        "#include<stdio.h>;",
    ),
    (
        "malloc",
        " extern void *malloc (size_t __size __attribute__ ((__nothrow__ , __leaf__))"
        + " __attribute__ ((__malloc__)));",
        "#include<stdlib.h>;",
    ),
    (
        "memcpy",
        " extern void *memcpy (void *__restrict __dest, const void *__restrict __src, size_t __n)"
        + " __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));",
        "#include<stdlib.h>;",
    ),
    (
        "strcpy",
        " extern char *strcpy (char *__restrict __dest, const char *__restrict __src)"
        + " __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));",
        "#include<string.h>;",
    ),
    (
        "strcat",
        " extern char *strcat (char *__restrict __dest, const char *__restrict __src)"
        + " __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));",
        "#include<string.h>;",
    ),
]


class TestVector:
    """Test vector.

    Consists of a unique name, the original file that
    describes the test vector,
    and the vector as a sequence of test inputs.
    Each test input is a dictionary and consists
    of a 'value' and a 'name'.
    """

    def __init__(self, name, origin_file):
        self.name = name
        self.origin = origin_file
        self._vector = list()

    def add(self, value, method=None):
        self._vector.append({"value": value, "name": method})

    @property
    def vector(self):
        """The sequence of test inputs of this test vector.

        Each element of this sequence is a dict
        and consists of two entries: 'value' and 'name'.
        The 'value' entry describes the input value, as it should be given
        to the program as input.
        The 'name' entry describes the program input method
        through which the value is retrieved. The value of this entry may be None.
        """
        return self._vector

    def __len__(self):
        return len(self.vector)

    def __repr__(self):
        return "%s: %s" % (self.origin, self._vector)

    def __str__(self):
        return self.origin


class TestResult:
    def __init__(self, verdict, execution_info, coverage=None):
        assert isinstance(verdict, str)
        self.verdict = verdict
        self.execution_info = execution_info
        self.coverage = coverage

    def __eq__(self, other):
        return self.verdict == other

    def __str__(self):
        return self.verdict

    def __repr__(self):
        return "(%s, %s, %s)" % (self.verdict, self.execution_info, self.coverage)


COVERS = "false"
UNKNOWN = "unknown"
ERROR = "error"
ABORTED = "abort"


class SuiteExecutionResult:
    """Results of a full test suite execution."""

    def __init__(self):
        self.results: List[ExecutionResult] = list()
        self.coverage_total = None
        self.successful_tests = list()
        self.coverage_sequence: List[float] = list()
        self.coverage_tests = list()
        self.reduced_coverage_tests = list()
        self.tests = list()


class ExecutionResult:
    """Results of a subprocess execution."""

    def __init__(
        self, returncode, stdout, stderr, got_aborted, cpu_time, wall_time, memory_used
    ):
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr
        self.got_aborted = got_aborted
        self.cpu_time = cpu_time
        self.wall_time = wall_time
        self.memory_used = memory_used

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, self.__hash__())

    def __str__(self):
        return "returncode %s" % self.returncode


def execute(command, quiet=False, input_str=None, timelimit=None):
    def shut_down(process):
        process.kill()
        return process.wait()

    log_cmd = logging.debug if quiet else logging.info
    log_cmd(" ".join(command))

    wall_time_start = time.perf_counter()
    process = subprocess.Popen(
        command,
        stdin=subprocess.PIPE if input_str else None,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=False,
    )

    output = None
    err_output = None
    wall_time = None
    try:
        if input_str and not isinstance(input_str, bytes):
            input_str = input_str.encode()
        output, err_output = process.communicate(
            input=input_str, timeout=timelimit if timelimit else None
        )
        returncode = process.poll()
        got_aborted = False
    except subprocess.TimeoutExpired:
        logging.debug("Timeout of %ss expired. Killing process.", timelimit)
        returncode = shut_down(process)
        got_aborted = True
    wall_time = time.perf_counter() - wall_time_start

    # We decode output, but we can't decode error output, since it may contain undecodable bytes.
    try:
        output = output.decode() if output else ""
    except UnicodeDecodeError:
        pass  # fail silently, continue with encoded output

    if output:
        logging.debug("Output of execution:\n%s", output)
    if err_output:
        try:
            err_output_for_msg = err_output.decode() if err_output else ""
        except UnicodeDecodeError:
            pass  # fail silently, continue with encoded output
        logging.debug("Error output of execution:\n%s", err_output_for_msg)

    return ExecutionResult(
        returncode, output, err_output, got_aborted, None, wall_time, None
    )


def found_err(output):
    return output and ERROR_STRING in str(output)
