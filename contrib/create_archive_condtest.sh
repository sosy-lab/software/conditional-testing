#!/bin/bash

# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -eao pipefail
IFS=$'\t\n'

NAME=condtest

DIRNAME="$(dirname "$(readlink -f "$0")")/.."
VERSION=$(git describe --always --dirty)
TMPDIR=$(mktemp -d)
pushd "$TMPDIR" > /dev/null
ln -s "$DIRNAME" "$NAME"
find "$NAME"/condtest -name '*.py' -exec sed -i "s/\(__VERSION__\s*=\s*\).*/\1\"$VERSION\"/" '{}' +
zip --exclude="*/test/*" --exclude="*/a.out" --exclude="*/.idea/*" --exclude="*/__pycache__/*" -r "$NAME".zip "$NAME"/{bin,condtest,lib,LICENSE.txt,README.md,tool_templates,tools}
popd
mv "$TMPDIR/$NAME.zip" ./
echo "Wrote $NAME.zip, version $VERSION"
