# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import _util as u
import re
import tempfile
import subprocess
from typing import List, Sequence
from nose.tools import eq_
from condtest import reducer
from condtest.reducer import identity, annotation, syntactic_prune

LABEL_FILE_WITH_ALL_COVERED = u.test_dir() / "goal0-6.txt"
LABEL_FILE_WITH_FIRST_GOAL_COVERED = u.test_dir() / "goal3.txt"
LABEL_FILE_WITH_ONE_PATH_COVERED = u.test_dir() / "goal2-4.txt"

LABEL_FILES = (
    None,
    LABEL_FILE_WITH_ALL_COVERED,
    LABEL_FILE_WITH_FIRST_GOAL_COVERED,
    LABEL_FILE_WITH_ONE_PATH_COVERED,
)

FIRST_GOAL = u.get_goal(3)
GOALS_ON_PATH_COVERED = [u.get_goal(number) for number in (2, 3, 4)]


def _get_arguments(output_file=None, label_file=None) -> List[str]:
    arguments = u.cli_arguments(u.instrumented_program())
    if output_file:
        arguments += ["--output", output_file]
    if label_file:
        arguments += ["--covered-labels", str(label_file)]
    return arguments


def test_module_simple_program_parsable():
    for reducer_class in (
        identity.IdentityReducer,
        annotation.AnnotatingReducer,
        syntactic_prune.PruningReducer,
    ):
        for label_file in LABEL_FILES:
            yield _check_module_simple_program_result_is_parsable, reducer_class, label_file


def _check_module_simple_program_result_is_parsable(reducer_class, label_file=None):
    residual_content = _run_module_simple_program(reducer_class, label_file)

    u.check_program_parsable(residual_content)


def test_module_simple_program_identity_always_same():
    for label_file in LABEL_FILES:
        yield _check_module_simple_program_identity_same, label_file


def _check_module_simple_program_identity_same(label_file):
    with open(u.instrumented_program()) as inp:
        input_content = inp.read()

    residual_content = _run_module_simple_program(identity.IdentityReducer, label_file)

    u.check_content_equal(residual_content, input_content)


def test_module_simple_program_annotator_all_covered_is_same():
    with open(u.instrumented_program()) as inp:
        input_content = inp.read()

    residual_content = _run_module_simple_program(
        annotation.AnnotatingReducer, LABEL_FILE_WITH_ALL_COVERED
    )

    u.check_content_equal(residual_content, input_content)


def test_module_simple_program_annotator_none_covered():
    residual_content = _run_module_simple_program(
        annotation.AnnotatingReducer, label_file=None
    )

    _check_each_label_annotated(residual_content)


def test_module_simple_program_annotator_only_intermediate_goal_covered():
    residual_content = _run_module_simple_program(
        annotation.AnnotatingReducer, label_file=LABEL_FILE_WITH_FIRST_GOAL_COVERED
    )

    for line in residual_content.split("\n"):
        if FIRST_GOAL in line:
            assert not _is_annotated_or_no_goal(line), line
        else:
            assert _is_annotated_or_no_goal(line), line


def test_module_simple_program_annotator_full_path_covered():
    residual_content = _run_module_simple_program(
        annotation.AnnotatingReducer, label_file=LABEL_FILE_WITH_ONE_PATH_COVERED
    )

    for line in residual_content.split("\n"):
        if any(g in line for g in GOALS_ON_PATH_COVERED):
            assert not _is_annotated_or_no_goal(line), line
        else:
            assert _is_annotated_or_no_goal(line), line


def _check_each_label_annotated(content):
    for line in content.split("\n"):
        assert _is_annotated_or_no_goal(line)


def _is_annotated_or_no_goal(line):
    free_label_regex = re.compile(r"\s*" + u.LABEL_PREFIX + r"[0-9]+:\s*;?$")
    return not free_label_regex.match(line)


def _is_followed_by_abort_or_no_goal(line):
    free_label_regex = re.compile(r"\s*" + u.LABEL_PREFIX + r"[0-9]+:\s*;?$")
    return not free_label_regex.match(line)


def test_module_simple_program_pruner_none_covered_is_same():
    with open(u.instrumented_program()) as inp:
        input_content = inp.read()

    residual_content = _run_module_simple_program(
        syntactic_prune.PruningReducer, label_file=None
    )

    u.check_content_equal(residual_content, input_content)


def _is_goal_label(line):
    return u.LABEL_PREFIX in line


def _is_abort(line):
    return "abort();" in line


def _check_goals_cut_off(content, condition=_is_goal_label):
    # the following check ignores the case that the very last line of the residual program is a test goal,
    # but this is also not possible for valid C programs.
    lines = content.split("\n")
    line_pairs = zip(
        lines, lines[1:]
    )  # [(line1, line2), (line2, line3), ..., (line n-1, line n)]
    for line1, line2 in line_pairs:
        assert condition(line1) == _is_abort(line2), "%s\n%s" % (line1, line2)


def test_module_simple_program_pruner_all_covered():
    residual_content = _run_module_simple_program(
        syntactic_prune.PruningReducer, label_file=LABEL_FILE_WITH_ALL_COVERED
    )

    _check_goals_cut_off(residual_content)


def test_module_simple_program_pruner_only_intermediate_goal_covered():
    with open(u.instrumented_program()) as inp:
        input_content = inp.read()

    residual_content = _run_module_simple_program(
        syntactic_prune.PruningReducer, label_file=None
    )

    u.check_content_equal(residual_content, input_content)


def test_module_simple_program_pruner_full_path_covered():
    residual_content = _run_module_simple_program(
        syntactic_prune.PruningReducer, label_file=LABEL_FILE_WITH_ONE_PATH_COVERED
    )

    _check_goals_cut_off(residual_content, lambda line: u.LABEL_PREFIX + "4:" in line)


def _run_module_simple_program(reducer_class, label_file) -> str:
    with tempfile.NamedTemporaryFile(mode="r") as output_target:
        arguments = _get_arguments(output_target.name, label_file)
        reducer.main(reducer_class, arguments)

        return output_target.read()


def _run_executable_simple_program(executable, label_file) -> str:
    arguments = [executable] + _get_arguments(label_file=label_file)

    return subprocess.check_output(arguments).decode()


def _binaries() -> Sequence[str]:
    reducer_dir = u.test_dir().parent / "bin" / "reducer"
    return (
        str(reducer_dir / "identity"),
        str(reducer_dir / "annotator"),
        str(reducer_dir / "pruner"),
    )


def test_executable_simple_program():
    for executable in _binaries():
        for label_file in LABEL_FILES:
            yield _check_executable_simple_program_result_is_parsable, executable, label_file


def _check_executable_simple_program_result_is_parsable(executable, label_file=None):
    residual_content = _run_executable_simple_program(executable, label_file)

    u.check_program_parsable(residual_content)


def _check_version(executable):
    arguments = [executable, "--version"]
    output = subprocess.check_output(arguments).decode()

    lines = output.split("\n")
    eq_(len(lines), 2)
    assert not lines[1]


def test_version():
    for executable in _binaries():
        yield _check_version, executable
