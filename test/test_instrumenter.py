# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from pathlib import Path
import re
import subprocess
import tempfile
import _util as u
from nose.tools import eq_
from condtest import instrumenter

EXPECTED_TESTGOAL_COUNT = 6


def _check_testgoal_count(instrumented, expected_count):
    goal_regex = re.compile(u.LABEL_PREFIX + r"[0-9]+:")
    lines = instrumented.split("\n")
    goal_count = sum(1 for line in lines if goal_regex.search(line))

    eq_(goal_count, expected_count)


def test_module_simple_program():
    with tempfile.NamedTemporaryFile(mode="r") as output_target:
        arguments = ["--output", output_target.name] + u.cli_arguments(
            u.original_program()
        )

        instrumenter.main(arguments)
        instrumented_content = output_target.read()

    _check_testgoal_count(instrumented_content, EXPECTED_TESTGOAL_COUNT)
    u.check_program_parsable(instrumented_content)


def test_instrumentation_simple_program():
    arguments = u.cli_arguments(u.original_program())
    # we don't really need these arguments, but parse them to get a valid args object
    args = instrumenter._parse_args(arguments)

    # if we pass a str here instead of a Path, the instrumenter will try to parse the str
    # as program content
    instrumented_content = instrumenter.instrument(Path(u.original_program()), args)

    _check_testgoal_count(instrumented_content, EXPECTED_TESTGOAL_COUNT)
    u.check_program_parsable(instrumented_content)


def _binary() -> str:
    return str(u.test_dir().parent / "bin" / "instrumenter" / "instrumenter")


def test_executable_simple_program():
    arguments = [_binary(), "--spec", u.spec_file(), u.original_program()]

    instrumented_content = subprocess.check_output(arguments).decode()

    _check_testgoal_count(instrumented_content, EXPECTED_TESTGOAL_COUNT)
    u.check_program_parsable(instrumented_content)


def test_version():
    arguments = [_binary(), "--version"]
    output = subprocess.check_output(arguments).decode()

    lines = output.split("\n")
    eq_(len(lines), 2)
    assert not lines[1]
