# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from pathlib import Path
import re
from collections import namedtuple
import pycparser
from nose.tools import eq_


LABEL_PREFIX = "GOAL_"


def test_dir() -> Path:
    return Path(__file__).parent


def spec_file() -> str:
    return str(test_dir() / "coverage-branches.prp")


def original_program() -> str:
    return str(test_dir() / "simple.c")


def instrumented_program() -> str:
    return str(test_dir() / "simple.instrumented.c")


def get_goal(number):
    return LABEL_PREFIX + str(number)


def check_program_parsable(content):
    parser = pycparser.c_parser.CParser()
    parser.parse(content)


def check_content_equal(actual_content, expected_content):
    Deviation = namedtuple("Deviation", ("line_number", "actual", "expected"))
    actual_lines = actual_content.split("\n")
    expected_lines = expected_content.split("\n")

    deviations = list()
    for line_number, (actual, expected) in enumerate(
        zip(actual_lines, expected_lines), 1
    ):
        if actual != expected:
            deviations.append(Deviation(line_number, actual, expected))

    if deviations:
        error_string = "\n".join(
            ["Content not equal:"]
            + [
                str(d[0]) + ": " + '"' + d[1] + '" != "' + d[2] + '"'
                for d in deviations[:10]
            ]
        )
        raise AssertionError(error_string)


def cli_arguments(program_file):
    return ["--spec", spec_file(), program_file]
