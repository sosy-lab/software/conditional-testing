# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import _util as u
import re
import tempfile
import subprocess
from typing import List, Sequence
from nose.tools import eq_
from condtest import goal_extractor

TEST_SUITE_SIMPLE = u.test_dir() / "simple.test-suite.zip"


def _binary() -> str:
    return str(u.test_dir().parent / "bin" / "extractor" / "test_executor")


def _check_goals_covered(goal_content, expected_goals):
    goals = goal_content.strip().split("\n")
    assert len(goals) == len(expected_goals) and all(
        g in goals for g in expected_goals
    ), "%s != %s" % (goals, expected_goals)


def _run_executable_simple_program(executable, program_file, test_suite, opts=[]):
    arguments = (
        [executable, "--test-suite", test_suite] + u.cli_arguments(program_file) + opts
    )
    return subprocess.check_output(arguments).decode()


def test_executable_simple_program_output_to_stdout():
    program = u.instrumented_program()
    test_suite = str(TEST_SUITE_SIMPLE)
    expected_goals_covered = [u.get_goal(n) for n in (3, 6)]

    covered_goals = _run_executable_simple_program(_binary(), program, test_suite)

    _check_goals_covered(covered_goals, expected_goals_covered)


def test_executable_simple_program_output_to_file():
    program = u.instrumented_program()
    test_suite = str(TEST_SUITE_SIMPLE)
    expected_goals_covered = [u.get_goal(n) for n in (3, 6)]
    with tempfile.NamedTemporaryFile(mode="r") as output_target:
        output_arguments = ["--output", output_target.name]

        _run_executable_simple_program(
            _binary(), program, test_suite, opts=output_arguments
        )

        covered_goals = output_target.read()

    _check_goals_covered(covered_goals, expected_goals_covered)


def test_version():
    arguments = [_binary(), "--version"]
    output = subprocess.check_output(arguments).decode()

    lines = output.split("\n")
    eq_(len(lines), 2)
    assert not lines[1]
