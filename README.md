<!--
This file is part of CondTest,
a collection of example implementations for conditional testing:
https://gitlab.com/sosy-lab/software/conditional-testing

SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Conditional Testing Framework (CondTest)

[![Apache 2.0 License](https://img.shields.io/badge/license-Apache--2-brightgreen.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)

CondTest can be used to combine different automatic test generators
to improve test-suite generation.
It is meant as a proof of concept for conditional software testing,
as described in our publication:

[Dirk Beyer and Thomas Lemberger: *Conditional Testing - Off-the-Shelf Combination of Test-Case Generators.* ATVA 2019.](https://www.sosy-lab.org/research/pub/2019-ATVA.Conditional_Testing_Off-the-Shelf_Combination_of_Test-Case_Generators.pdf)

## Requirements

Software required:

- `BenchExec >= 1.20`
- `lcov >= 1.13`

Python packages required:

- `psutil >= 5.6.2`
- `pycparser >= 2.19`

They can be installed through pip or by running `./setup.py install`.

## Branch information

[**Before version 3.0**](https://gitlab.com/sosy-lab/software/conditional-testing/-/tree/v2.0),
this repository contained each component of CondTest as
more or less coupled python modules, that are combined through glue code.

**Since version 3.0**, this repository contains each component of CondTest as
an individual executable refactored from the original code.
This allows users to use each component individually without programming knowledge.
No glue code is provided, though, and combinations must be done through
own languages or scripts.

**[CoVeriTeam](https://gitlab.com/sosy-lab/software/coveriteam) solves this:**
CoVeriTeam is a tool to execute compositions of verification tools,
and it already contains an implementation and examples for CondTest.
Take a look there if you want to combine the components of this repository
and run conditional testing.

## Basic Usage

The following executables and components are implemented (order as in our paper)

Component               | Executable
:------------           | :-------------
Program Instrumentation | `bin/instrumenter/instrumenter`
Identity Reducer        | `bin/reducer/identity`
Pruning Reducer         | `bin/reducer/pruning`
Annotating Reducer      | `bin/reducer/annotation`
Test-Goal Extractor     | `bin/extractor/execution`

The program instrumentation is not part of the paper, but instruments a given input program
with test-goal labels. These labels are used by the remaining components to identify test goals
covered/remaining.


Run `EXECUTABLE --help` to see all available command-line arguments.

Argument `--spec` is always required to define the test-goal specification.
At the moment, only `cover-branches.prp` is supported.

# License

Most files in this project are licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0)
and have copyright by [Dirk Beyer](https://www.sosy-lab.org/people/beyer/).